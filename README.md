# Bitbucket Pipelines Pipe: Bitbucket Pipe release

This pipe simplifies the release process of the Bitbucket Pipe or any Bitbucket Cloud project by abstracting away the version management, 
docker image build and push process, commiting and tagging new version of the pipe or other project.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:           
    
```yaml
- pipe: atlassian/bitbucket-pipe-release:4.2.30
  variables:
    DOCKERHUB_USERNAME: '<string>'
    DOCKERHUB_PASSWORD: '<string>'
    IMAGE: '<string>'
    # VERSION: '<string>' # Optional
    # CHANGELOG: '<boolean>' # Optional
    # UPDATE_VERSION_IN_FILES: '<string>' # Optional
    # GIT_PUSH: '<boolean>' # Optional
    # GIT_USER: '<string>' # Optional
    # SSH_KEY: '<string>' # Optional
    # TAG: '<boolean>' # Optional
    # WITH_DIGEST: '<boolean>' # Optional
```

## Variables

| Variable                | Usage                                                |
| ------------------------| ---------------------------------------------------- |
| DOCKERHUB_USERNAME (*)  | Dockerhub username. |
| DOCKERHUB_PASSWORD (*)  | Dockerhub password or access token. |
| IMAGE (*)               | The name of an image to be released. |
| VERSION                 | New pipe version identifier. Default: `null`. If no value provided, the pipe will use `semversioner` tool to manage versions.|
| CHANGELOG               | Boolean flag to indicate whether you want the pipe to generate a changelog using `semversioner`. Default `true`. |
| UPDATE_VERSION_IN_FILES | List of white space separated filenames in whish you want to replace the old version with the new one. 
|                         | Default `pipe.yml README.md` |
| GIT_PUSH                | Flag to tell the pipe to create a new commit with updated files. Default `true`. |
| GIT_USER                | Name of the user that have push access to repository. Default: `Bitbucket Pipelines Push Bot` is used. |
| SSH_KEY                 | The ssh private key configured for git account in format of base64 encoded key string. |
| TAG                     | Flag to tell the pipe to create a git tag with the new version identifier. Default `true`. If used, the `GIT_PUSH` should be also enabled. |
| WITH_DIGEST             | Add digest to metadata image for current repository tag bumped. Default: `false`. If enabled, `TAG` and `GIT_PUSH` should be enabled. 

_(*) = required variable._

## Details
For authorized pushing to git several approaches can be used:

  * (recommended) Configure repository ssh key pair and it will be safely used in pipeline to push to current repository. See details in [documentation][use-ssh-keys-in-pipelines]; 
  * if configured SSH_KEY environment variable, it will be used to push. To create ssh key [guide to set base64 encoded environment variables][ssh-set-private-key];  
  * if neither of above is configured, the http proxy and http origin will be used to push. See details in [docs to push back to repository][using-http-proxy-to-push].

The most secured way is using first way with repository ssh keys, as private key is not exposed anywhere out of pipeline.
Be aware, that the public key of generated keypair must be added to authorized keys in account(s) of that user(s) who must have write access to master.


## Examples

### Basic example:

Example releasing a new version of the pipe:
    
```yaml
script:
  - pipe: atlassian/bitbucket-pipe-release:4.2.30
    variables:
      DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
      DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
      IMAGE: my-dockerhub-namespace/my-repo

```

Or if used for current repository, then:

```yaml
script:
  - pipe: atlassian/bitbucket-pipe-release:4.2.30
    variables:
      DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
      DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
      IMAGE: my-dockerhub-namespace/$BITBUCKET_REPO_SLUG

```


### Advanced examples

The following example demonstrates how you might release a development version -docker image - for testing.
Here `${BITBUCKET_BUILD_NUMBER}` is used to generate a unique pipe version on every new build. 
Also, since a new commit and tag for the development release should not be created, `GIT_PUSH` is set to `false`. By default it is `true`.
If `GIT_PUSH` is disabled, then `TAG` option is disabled also, since `GIT_PUSH` causes some changes to tag.

```yaml
script:
  - pipe: atlassian/bitbucket-pipe-release:4.2.30
    variables:
      DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
      DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
      IMAGE: my-dockerhub-repository/my-pipe
      VERSION: test-${BITBUCKET_BUILD_NUMBER}
      GIT_PUSH: 'false'
```

To deploy to specific branch that is different from current on your pipeline you need to specify BITBUCKET_BRANCH global variable explicitly:
```yaml
step:
  name: Release for QA testing
  script:
    - pipe: atlassian/bitbucket-pipe-release:4.2.30
      variables:
        DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
        DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
        IMAGE: my-dockerhub-repository/my-pipe
        VERSION: qa-${BITBUCKET_BUILD_NUMBER}
        BITBUCKET_BRANCH: qa-branch
```

To add a current image digest to pipe metadata and increase the security value (specific image with appropriate hash will be always pulled on pipe execution
and image in pipe.yml will have format in appropriate git tag - <dockerhub-repository>/<image>:<version>@sha256:<hash>):

```yaml
step:
  name: Release for QA testing
  script:
    - pipe: atlassian/bitbucket-pipe-release:4.2.30
      variables:
        DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
        DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
        IMAGE: my-dockerhub-repository/my-pipe
        WITH_DIGEST: 'true'
```

See more details in [documentation for default pipelines variables][default-pipelines-variables].


### Additional details

In this pipe such global variables are used (You can change them passing to the pipe in hardcoded way):

* BITBUCKET_BRANCH which is by default branch of current pipe execution
* BITBUCKET_GIT_SSH_ORIGIN which is by default ssh origin url of current repository where pipe is executed 
* BITBUCKET_GIT_HTTP_ORIGIN which is by default http origin url of current repository where pipe is executed
* BITBUCKET_BUILD_NUMBER which is by default number of current pipeline execution


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes
[ssh-set-private-key]: https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html#UseSSHkeysinBitbucketPipelines-UsemultipleSSHkeysinyourpipeline
[use-ssh-keys-in-pipelines]: https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html
[using-http-proxy-to-push]: https://confluence.atlassian.com/bitbucket/push-back-to-your-repository-962352710.html
[default-pipelines-variables]: https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html#DefaultVariables
