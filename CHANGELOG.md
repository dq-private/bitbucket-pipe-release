# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 4.2.30

- patch: remove dind and services

## 4.2.29

- patch: Install buildx
- patch: Install buildx
- patch: Install buildx

## 4.2.28

- patch: Add buildx install
- patch: Update docker compose

## 4.2.27

- patch: Add buildx

## 4.2.26

- patch: Add config for buildx container
- patch: Add config for buildx container

## 4.2.25

- patch: Update buildx
- patch: fix url

## 4.2.24

- patch: fix

## 4.2.23

- patch: fix

## 4.2.22

- patch: debug

## 4.2.21

- patch: debug

## 4.2.20

- patch: debug

## 4.2.19

- patch: debug nog een keer

## 4.2.18

- patch: len

## 4.2.17

- patch: append to array

## 4.2.16

- patch: append to array

## 4.2.15

- patch: add debug

## 4.2.14

- patch: make array

## 4.2.13

- patch: add debug

## 4.2.12

- patch: add debug

## 4.2.11

- patch: add debug

## 4.2.10

- patch: convert stringable array to python list

## 4.2.9

- patch: convert stringable array to python list

## 4.2.8

- patch: add buildx
- patch: buildx

## 4.2.7

- patch: add buildx
- patch: update python docker

## 4.2.6

- patch: typo

## 4.2.5

- patch: use docker on whales api

## 4.2.4

- patch: use items as array

## 4.2.3

- patch: use items as array

## 4.2.2

- patch: use list not array

## 4.2.1

- patch: skip tests

## 4.2.0

- minor: Bump semversioner vesion.
- patch: Improve semversioner logs: add source from where log came.

## 4.1.0

- minor: Bump bitbucket-pipes-toolkit -> 2.2.0.

## 4.0.1

- patch: Add category to pipe's metadata.

## 4.0.0

- major: Initial release: Pipe-release from internal to public.

## 3.2.2

- patch: Internal maintenance: fix regular expression to update version properly.

## 0.1.0

- minor: Pipe release.

