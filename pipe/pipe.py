import glob
import re
import os
import subprocess
import stat

from python_on_whales import docker
from bitbucket_pipes_toolkit import Pipe

SCHEMA = {
    # GLOBALS
    'BITBUCKET_BRANCH': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_SSH_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_GIT_HTTP_ORIGIN': {'required': True, 'type': 'string'},
    'BITBUCKET_BUILD_NUMBER': {'required': True, 'type': 'integer'},

    # PIPE-SPECIFIC
    'IMAGE': {'required': True, 'type': 'string'},
    'DOCKERHUB_USERNAME': {'required': True, 'type': 'string'},
    'DOCKERHUB_PASSWORD': {'required': True, 'type': 'string'},
    'UPDATE_VERSION_IN_FILES': {'required': False, 'type': 'string', 'default': 'README.md pipe.yml'},
    'VERSION': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'CHANGELOG': {'required': False, 'type': 'boolean', 'default': True},
    'GIT_PUSH': {'required': False, 'type': 'boolean', 'default': True},
    'GIT_USER': {'required': False, 'type': 'string', 'default': 'Bitbucket Pipelines Push Bot'},
    'SSH_KEY': {'required': False, 'type': 'string', 'default': None, 'nullable': True},
    'TAG': {'required': False, 'type': 'boolean', 'default': True},
    'PLATFORMS': {'required': False, 'type': 'list', 'default': ['linux/amd64']},
    'WITH_DIGEST': {'required': False, 'type': 'boolean', 'default': False},
}


def replace_content_by_pattern(filename, replacement_string, pattern):
    with open(filename) as f:
        content = f.read()

    with open(filename, 'w') as f:
        new_content = re.sub(re.compile(pattern), replacement_string, content)
        f.write(new_content)


class Release(Pipe):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.image = self.get_variable('IMAGE')

        self._docker_client = None
        self._script_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'scripts')
        self._allow_executable()

    def run(self):
        super().run()

        version = self.version_bump()
        tags = self.generate_tags(version)
        self.log_info(f"Generated tags {tags}")
        image_digest = self.docker_release(tags)

        if 'latest' in tags:
            tags.remove('latest')

        if self.get_variable('GIT_PUSH'):
            self.log_info(f'Committing version {version} to git')
            self.git_commit(version)
        if self.get_variable('WITH_DIGEST'):
            replace_content_by_pattern('pipe.yml', f'{version}@{image_digest}', version)

        if self.get_variable('TAG') and self.get_variable('GIT_PUSH'):
            self.log_info(f'Tag version {version} to git')
            self.git_tag(version)

        self.success('Release pipe finished successfully')

    @property
    def docker(self):
        if self._docker_client is None:
            self._docker_client = docker
        return self._docker_client

    def _allow_executable(self):
        for path in glob.iglob(os.path.join(self._script_path, '*.sh')):
            os.chmod(path, os.stat(path).st_mode | stat.S_IEXEC)

    def version_bump(self):
        """Bump a version.

        Bump a version, update necessary files, generate changelog
        """
        # TODO: get rid of subprocess calling, once semversioner supports python functions outside
        version = self.get_variable('VERSION')
        if not version:
            try:
                subprocess.run(['semversioner', 'release'], check=True, capture_output=True, text=True)
                result = subprocess.run(['semversioner', 'current-version'], check=True, capture_output=True, text=True)
                version = result.stdout.strip('\n')
            except subprocess.CalledProcessError as exc:
                self.fail(f'Semversioner. {exc.output} {exc.stderr}')

        if self.get_variable('CHANGELOG'):
            self.log_info('Generating CHANGELOG.md file...')
            try:
                result = subprocess.run(['semversioner', 'changelog'],
                                        check=True, capture_output=True, text=True)
                with open('CHANGELOG.md', 'w') as f:
                    f.write(result.stdout)
            except subprocess.CalledProcessError as exc:
                self.fail(f'Semversioner. {exc.output} {exc.stderr}')

        filenames_to_update = self.get_variable('UPDATE_VERSION_IN_FILES').split(' ')
        if filenames_to_update == '':
            return

        for filename in filenames_to_update:
            repo_slug = self.image.split('/')[-1]
            replace_content_by_pattern(filename, f'{repo_slug}:{version}',
                                       f'{repo_slug}:[0-9]*\.[0-9]*\.[0-9]*(@sha256:[a-fA-F0-9]*)?') # noqa

        return version

    def generate_tags(self, version):
        branch = self.get_variable('BITBUCKET_BRANCH')
        if branch == 'master':
            return ['latest', version]
        else:
            return [version]

    def docker_release(self, tags):
        docker.login(username=self.get_variable('DOCKERHUB_USERNAME'), password=self.get_variable('DOCKERHUB_PASSWORD'))
        self.log_info('Building docker image')
        intermediate_tag = [f'{self.image}:pipelines-{self.get_variable("BITBUCKET_BUILD_NUMBER")}']
        platforms = self.get_variable('PLATFORMS').decompile()

        build_for_platforms = []
        for i in range(int(platforms['PLATFORMS_COUNT'])):
            self.log_info(platforms[f'PLATFORMS_{i}'])
            build_for_platforms.append(platforms[f'PLATFORMS_{i}'])

        self.log_info(build_for_platforms)
        image, _ = docker.buildx.build(context_path='.', tags=intermediate_tag, platforms=build_for_platforms, cache=False )

        # NOTE: we take the digest of version last in a list pushed
        image_data = None
        for tag in tags:
            image.tag(self.image, tag=tag)
            self.log_info(f'Tagged image {self.image}:{tag}')

            output = docker.image.push(self.image)
            for line in output:
                self.log_info(line)
            image_data = docker.image.inspect(f'{self.image}:{tag}')

        return image_data['RepoDigests'][0].split('@')[-1]

    def git_commit(self, version):
        script = os.path.join(self._script_path, 'git-commit.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')

    def git_tag(self, version):
        script = os.path.join(self._script_path, 'git-tag.sh')
        try:
            subprocess.run([script, version], check=True, capture_output=True, text=True)
        except subprocess.CalledProcessError as exc:
            self.fail(f'{exc.output} {exc.stderr}')


def main():
    release = Release(schema=SCHEMA)
    release.run()


if __name__ == '__main__':
    main()
