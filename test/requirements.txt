bitbucket-pipes-toolkit==2.2.0
semversioner==0.8.1
pytest==4.3.0
flake8==3.7.7
python-on-whales==0.41.0
