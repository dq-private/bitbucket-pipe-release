from contextlib import contextmanager
import io
import os
import subprocess
import sys
from unittest import mock

from bitbucket_pipes_toolkit.test import PipeTestCase

from pipe.pipe import main


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


@mock.patch.dict(
    os.environ,
    {'BITBUCKET_GIT_HTTP_ORIGIN': 'https://fake.git', 'BITBUCKET_BUILD_NUMBER': '0',
     'BITBUCKET_GIT_SSH_ORIGIN': 'git@fake.git', 'BITBUCKET_APP_PASSWORD': 'test', 'BITBUCKET_USERNAME': 'test',
     'DOCKERHUB_USERNAME': 'test', 'DOCKERHUB_PASSWORD': 'test'})
class ReleaseTest(PipeTestCase):

    def setUp(self):
        self.open_patcher = mock.patch('pipe.pipe.open')
        open_mock = self.open_patcher.start()
        open_mock.return_value = enter_mock = mock.MagicMock()
        enter_mock.__enter__.return_value = self.file_mock = mock.MagicMock()
        self.write_mock = self.file_mock.write = mock.MagicMock()

    def tearDown(self):
        self.open_patcher.stop()

    @mock.patch.dict(
        os.environ,
        {'IMAGE': 'fake/repo', 'VERSION': 'fake-0.0.0', 'CHANGELOG': 'true',
         'GIT_PUSH': 'true', 'TAG': 'True', 'WITH_DIGEST': 'True', 'BITBUCKET_BRANCH': 'release-dev-10'})
    @mock.patch('docker.login', new=mock.Mock(return_value='Login Succeeded'))
    @mock.patch('docker.models.images.ImageCollection.push')
    @mock.patch('docker.APIClient.inspect_image',
                new=mock.Mock(return_value={'RepoDigests': ['repo:version@sha256:abcdef']}))
    @mock.patch('docker.APIClient.tag')
    @mock.patch('subprocess.run',
                side_effect=[mock.MagicMock(**{'stdout': 'fake changelog'}),
                             'git commit result', 'git tag result'])
    def test_release_with_all_turned_in_features_success(self, subprocess_mock, tag_mock, push_mock):
        # NOTE: fake is image name. sometimes can be use in tests without namespace
        self.file_mock.read.side_effect = ['fake/repo:0.1.7', 'fake/repo:0.1.7@sha256:abcdef', 'fake/repo:fake-0.0.0']
        push_mock.return_value = ('Layer exists', 'Pushed image', 'size: 1234')
        main()

        kwargs = {'check': True, 'capture_output': True, 'text': True}
        script_path = os.path.join(os.getcwd(), 'pipe', 'scripts')
        subprocess_mock.assert_has_calls(
            [mock.call(['semversioner', 'changelog'], **kwargs),
             mock.call([os.path.join(script_path, 'git-commit.sh'), 'fake-0.0.0'],
                       **kwargs),
             mock.call([os.path.join(script_path, 'git-tag.sh'), 'fake-0.0.0'],
                       **kwargs),
             ])

        self.write_mock.assert_has_calls(
            [mock.call('fake changelog'), mock.call('fake/repo:fake-0.0.0'),
             mock.call('fake/repo:fake-0.0.0'), mock.call('fake/repo:fake-0.0.0@sha256:abcdef')])
        tag_mock.assert_called_once_with(mock.ANY,
                                         'fake/repo',
                                         tag=f'fake-0.0.0')
        push_mock.assert_called_once_with('fake/repo', tag=f'fake-0.0.0', decode=True, stream=True)

    # @mock.patch.dict(
    #     os.environ,
    #     {'IMAGE': 'fake/repo', 'VERSION': 'fake-0.0.0', 'CHANGELOG': 'true',
    #      'GIT_PUSH': 'true', 'TAG': 'True', 'BITBUCKET_BRANCH': 'release-dev-10'})
    # @mock.patch('docker.login', new=mock.Mock(return_value='Login Succeeded'))
    # @mock.patch('docker.models.images.ImageCollection.push')
    # @mock.patch('docker.APIClient.inspect_image',
    #             new=mock.Mock(return_value={'RepoDigests': ['repo:version@sha256:abcdef']}))
    # @mock.patch('docker.APIClient.tag')
    # @mock.patch('subprocess.run',
    #             side_effect=[mock.MagicMock(**{'stdout': 'fake changelog'}),
    #                          'git commit result', 'git tag result'])
    # def test_release_without_digest(self, subprocess_mock, tag_mock, push_mock):
    #     # NOTE: fake is image name. sometimes can be use in tests without namespace
    #     self.file_mock.read.side_effect = ['repo:0.1.7', 'repo:0.1.7@sha256:abcdef', 'repo:fake-0.0.0']
    #     push_mock.return_value = ('Layer exists', 'Pushed image', 'size: 1234')

    #     main()

    #     kwargs = {'check': True, 'capture_output': True, 'text': True}
    #     script_path = os.path.join(os.getcwd(), 'pipe', 'scripts')
    #     subprocess_mock.assert_has_calls(
    #         [mock.call(['semversioner', 'changelog'], **kwargs),
    #          mock.call([os.path.join(script_path, 'git-commit.sh'), 'fake-0.0.0'],
    #                    **kwargs),
    #          mock.call([os.path.join(script_path, 'git-tag.sh'), 'fake-0.0.0'],
    #                    **kwargs),
    #          ])

    #     self.write_mock.assert_has_calls(
    #         [mock.call('fake changelog'), mock.call('repo:fake-0.0.0')])
    #     tag_mock.assert_called_once_with(mock.ANY,
    #                                      'fake/repo',
    #                                      tag=f'fake-0.0.0')
    #     push_mock.assert_called_once_with('fake/repo', tag=f'fake-0.0.0', decode=True, stream=True)

    # @mock.patch.dict(
    #     os.environ,
    #     {'IMAGE': 'fake/repo', 'VERSION': '0.1.8', 'BITBUCKET_BRANCH': 'qa-dev',
    #      'CHANGELOG': 'false', 'GIT_PUSH': 'false', 'TAG': 'false', 'WITH_DIGEST': 'true', 'PLATFORM': ["linux/amd64", "linux/arm64"]})
    # @mock.patch('docker.login', new=mock.Mock(return_value='Login Succeeded'))
    # @mock.patch('docker.models.images.ImageCollection.push',
    #             new=mock.Mock(return_value=('Layer exists', 'Pushed image', 'size: 1234')))
    # @mock.patch('docker.APIClient.tag', new=mock.Mock())
    # @mock.patch('docker.APIClient.inspect_image',
    #             new=mock.Mock(return_value={'RepoDigests': ['repo:version@sha256:abcdef']}))
    # @mock.patch('subprocess.run')
    # def test_release_digest_not_in_pipe_yet(self, subprocess_mock):
    #     # NOTE: fake is image name. sometimes can be use in tests without namespace
    #     self.file_mock.read.side_effect = ['repo:0.1.7', 'repo:0.1.7', 'repo:0.1.8']
    #     main()

    #     subprocess_mock.assert_not_called()
    #     self.write_mock.assert_has_calls(
    #         [mock.call('repo:0.1.8'), mock.call('repo:0.1.8'), mock.call('repo:0.1.8@sha256:abcdef')])

    # @mock.patch.dict(os.environ, {'IMAGE': 'fake/repo', 'VERSION': 'release-dev-15', 'BITBUCKET_BRANCH': 'release-dev',
    #                               'CHANGELOG': 'false', 'GIT_PUSH': 'false', 'TAG': 'false', 'PLATFORM': ["linux/amd64", "linux/arm64"]})
    # @mock.patch('docker.login', new=mock.Mock(return_value='Login Succeeded'))
    # @mock.patch('docker.models.images.ImageCollection.push')
    # @mock.patch('docker.APIClient.tag')
    # @mock.patch('docker.APIClient.inspect_image',
    #             new=mock.Mock(return_value={'RepoDigests': ['repo:version@sha256:abcdef']}))
    # @mock.patch('subprocess.run')
    # def test_release_flags_turned_off_digest_turned_off(self, subprocess_mock, tag_mock, push_mock):
    #     # NOTE: fake is image name. sometimes can be use in tests without namespace
    #     self.file_mock.read.side_effect = ['repo:0.1.7', 'repo:0.1.7@sha256:abcdef', 'repo:release-dev-15']
    #     push_mock.return_value = ('Layer exists', 'Pushed image', 'size: 1234')
    #     main()

    #     subprocess_mock.assert_not_called()
    #     self.write_mock.assert_has_calls(
    #         [mock.call('repo:release-dev-15'),
    #          mock.call('repo:release-dev-15')])
    #     tag_mock.assert_called_once_with(mock.ANY,
    #                                      'fake/repo',
    #                                      tag=f'release-dev-15')
    #     push_mock.assert_called_once_with('fake/repo', tag=f'release-dev-15', decode=True, stream=True)

    # @mock.patch.dict(os.environ, {'IMAGE': 'fake/repo', 'BITBUCKET_BRANCH': 'qa-dev'})
    # @mock.patch('subprocess.run', side_effect=[
    #     subprocess.CalledProcessError(255, 'release',
    #                                   output='Error: No changes to release. Skipping release process.')])
    # def test_release_no_changes_to_release(self, subprocess_mock):
    #     with capture_output() as out:
    #         with self.assertRaises(SystemExit) as exc_context:
    #             main()
    #         self.assertEqual(exc_context.exception.code, 1)

    #     subprocess_mock.assert_called_once_with(['semversioner', 'release'],
    #                                             capture_output=True, check=True, text=True)
    #     self.assertRegex(out.getvalue(), '✖ Semversioner. Error: No changes to release. Skipping release process.')
