FROM python:3.9

COPY requirements.txt /
COPY LICENSE.txt pipe.yml README.md /

RUN apt-get update && pip install --no-cache-dir -r requirements.txt
RUN apt-get install -y ca-certificates curl gnupg lsb-release
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update
RUN apt-get install -y docker-ce docker-ce-cli containerd.io
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN python-on-whales download-cli

# install docker buildx, this step is optional
RUN mkdir -p ~/.docker/cli-plugins/
RUN wget https://github.com/docker/buildx/releases/download/v0.8.2/buildx-v0.8.2.linux-amd64 -O ~/.docker/cli-plugins/docker-buildx
RUN chmod a+x  ~/.docker/cli-plugins/docker-buildx

# install docker compose, this step is optional
RUN mkdir -p ~/.docker/cli-plugins/
RUN wget https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-linux-x86_64 -O ~/.docker/cli-plugins/docker-compose
RUN chmod a+x  ~/.docker/cli-plugins/docker-compose

RUN docker -v
RUN docker buildx install
RUN docker buildx version

COPY config/buildkit/buildkitd.toml /etc/buildkit/buildkitd.toml
COPY config/buildkit/buildkitd.toml ~/.config/buildkit/buildkitd.toml

COPY pipe /

ENTRYPOINT ["python", "/pipe.py"]
